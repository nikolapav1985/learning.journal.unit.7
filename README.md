Learning journal unit 7
-----------------------

- dct.py (dictionary examples)
- ./dct.py N (run example and pass a number as command line argument)
- couple of dictionaries produced as output
- second dictionary is inverse of first dictionary
- concrete example
- first dictionary, a square of numbers
- second dictionary, a square root of numbers

Test environment
----------------

os lubuntu 16.04 lts 4.13.0 kernel version 2.7.12 python version
